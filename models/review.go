package models

import (
	"../utils"
	"fmt"
	"github.com/lib/pq"
	"strconv"
	"time"
)

type Review struct {
	Review_id    int
	User_id      int
	Book_id      int
	Title        string
	Content      string
	Score        int
	Review_tag   []string
	Created_date time.Time
	Like         []int
	Comments     []int
}

func (review Review) GetReviewbyID(reviewID int) (err error) {

	fmt.Println("before query")
	query := fmt.Sprintf(`SELECT review_id, user_id, book_id, title, content, score, review_tag,
       								created_date, link, comments
								FROM test.reviews WHERE review_id = '%d'`, reviewID)

	rows, _ := utils.Db.Query(query)
	rows.Next()
	err = rows.Scan(&review.Review_id, &review.User_id, &review.Book_id, &review.Title,
		&review.Content, &review.Review_tag, &review.Review_tag, &review.Created_date, &review.Like,
		&review.Comments)

	fmt.Println("query: " + query)
	fmt.Println("after query")
	return
}

func (review *Review) SaveReview() (err error) {
	statement := `INSERT INTO test.reviews(
            review_id, user_id, book_id, title, content, score, review_tag,
            created_date, link, comments)
    VALUES ($1, $2, $3, $4, $5, $6, $7,
            $8, $9, $10)
    RETURNING review_id;`
	stmt, err := utils.Db.Prepare(statement)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	var created_review_id int
	err = stmt.QueryRow(review.Review_id, review.User_id, review.Book_id, review.Title,
		review.Content, review.Review_tag, pq.Array(review.Review_tag), review.Created_date, pq.Array(review.Like),
		pq.Array(review.Comments)).Scan(&created_review_id)
	fmt.Println("Created review id: " + strconv.Itoa(created_review_id))
	return
}

func (review *Review) UpdateReview() (err error) {
	statement := `UPDATE test.books
	SET user_id=$1, book_id=$2, title=$3, content=$4, score=$5, review_tag=$6, created_date=$7, link=$8, comments=$9
	WHERE review_id=$10`
	stmt, err := utils.Db.Prepare(statement)
	if err != nil {
		panic(err)
	}
	_, err = stmt.Exec(review.User_id, review.Book_id, review.Title,
		review.Content, review.Review_tag, pq.Array(review.Review_tag), review.Created_date, pq.Array(review.Like),
		pq.Array(review.Comments), review.Review_id)
	return
}
