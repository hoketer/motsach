package models

import (
	"../utils"
	"fmt"
	"github.com/lib/pq"
	"strconv"
	"time"
)

type Book struct {
	Book_ID        int
	Book_name      string
	Author_ID      int
	Author_name    string
	Description    string
	Published_date time.Time
	Pages          int
	ISBN           string
	Lang           string
	Tag            []string
	Category_ID    []int
	Category       []string
	Review_ID      []int
}

func (book *Book) GetBookbyID(bookID int) (err error) {
	fmt.Println("before query")
	query := fmt.Sprintf(`SELECT book_id, book_name, author_id, author_name,
									book_description, published_date, pages, isbn, lang,
									tag, category_id, category_name, review_id
								FROM test.books WHERE book_id = '%d'`, bookID)

	rows, _ := utils.Db.Query(query)
	rows.Next()
	err = rows.Scan(&book.Book_ID, &book.Book_name, &book.Author_ID, &book.Author_name,
		&book.Description, &book.Published_date, &book.Pages, &book.ISBN, &book.Lang,
		&book.Tag, &book.Category_ID, &book.Category, &book.Review_ID)

	fmt.Println("query: " + query)
	fmt.Println("after query")
	return
}

func (book *Book) SaveBook() (err error) {
	statement := `INSERT INTO test.books (book_id, book_name, author_id, author_name, book_description,
										published_date, pages, isbn, lang, tag, category_id, category_name, review_id)
				VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13) RETURNING book_id`
	stmt, err := utils.Db.Prepare(statement)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	var created_book_id int
	err = stmt.QueryRow(book.Book_ID, book.Book_name, book.Author_ID, book.Author_name,
		book.Description, book.Published_date, book.Pages, book.ISBN, book.Lang,
		pq.Array(book.Tag), pq.Array(book.Category_ID), pq.Array(book.Category), pq.Array(book.Review_ID)).Scan(&created_book_id)
	fmt.Println("Created book id: " + strconv.Itoa(created_book_id))
	return
}

func (book *Book) UpdateBook() (err error) {
	statement := `UPDATE test.books
	SET book_name=$1, author_id=$2, author_name=$3, book_description=$4, published_date=$5, pages=$6, isbn=$7, lang=$8, tag=$9, category_id=$10, category_name=$11, review_id=$12
	WHERE book_id=$13`
	stmt, err := utils.Db.Prepare(statement)
	if err != nil {
		panic(err)
	}
	_, err = stmt.Exec(book.Book_name, book.Author_ID, book.Author_name,
		book.Description, book.Published_date, book.Pages, book.ISBN, book.Lang,
		pq.Array(book.Tag), pq.Array(book.Category_ID), pq.Array(book.Category),
		pq.Array(book.Review_ID), book.Book_ID)
	return
}
