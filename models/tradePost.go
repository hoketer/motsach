package models

import (
	"time"
	"fmt"
	"../utils"
	"strconv"
)

type TradePost struct{
	TradePost_ID int
	Format string
	Price float32
	Description string
	Created_date time.Time
	User_ID int
	Book_ID []int
}

func (tradePost *TradePost) GetTradePostByID(tradePost_ID int) (err error){
	fmt.Println("before query")
	query := fmt.Sprintf(`SELECT tradepost_id, format, price, description, created_date, user_id, book_id
								FROM test.tradeposts WHERE tradepost_id = '%d'`, tradePost_ID)

	rows, _ := utils.Db.Query(query)
	rows.Next()
	err = rows.Scan(&tradePost.TradePost_ID, &tradePost.Format, &tradePost.Price, &tradePost.Description,
		&tradePost.Created_date, &tradePost.User_ID, &tradePost.Book_ID)

	fmt.Println("query: " + query)
	fmt.Println("after query")
	return
}

func (tradePost *TradePost) CreateTradePost() (err error){
	statement := `INSERT INTO test.tradeposts(
            tradepost_id, format, price, description, created_date, user_id, book_id)
    VALUES ($1, $2, $3, $4, $5, $6, $7)
    RETURNING review_id;`
	stmt, err := utils.Db.Prepare(statement)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	var createdTradePost int;
	err = stmt.QueryRow(tradePost.TradePost_ID, tradePost.Format, tradePost.Price, tradePost.Description,
		tradePost.Created_date, tradePost.User_ID, tradePost.Book_ID).Scan(&createdTradePost)
	fmt.Println("Created trade id: " + strconv.Itoa(createdTradePost))
	return
}

func (tradePost *TradePost) UpdateTradePost() (err error) {
	statement := `UPDATE test.tradeposts
	SET format=$1, price=$2, description=$3, created_date=$4, user_id=$5, book_id=$6
	WHERE tradepost_id=$7`
	stmt, err := utils.Db.Prepare(statement)
	if err != nil {
		panic(err)
	}
	_, err = stmt.Exec(tradePost.Format, tradePost.Price, tradePost.Description,
		tradePost.Created_date, tradePost.User_ID, tradePost.Book_ID, tradePost.TradePost_ID)
	return
}
