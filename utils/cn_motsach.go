package utils

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
)

var (
	Db *sql.DB
)

func init() {
	var err error
	Db, err = sql.Open("postgres", `user=postgres
						dbname=motsach password=motsach sslmode=disable`)
	if err != nil {
		panic(err)
	}
	fmt.Print("Init connect")
}
