package controllers

import (
	"../models"
	"../views"
	"net/http"

)

type UserController struct {
	models.TradePost
	views.TradePostView
}

func UserHandler (w http.ResponseWriter, r *http.Request) {
	uc := NewUserController()
	uc.ControlSelectReview(w, r)
}

func NewUserController() *UserController {
	return &UserController{}
}

func (uc UserController) ControlSelectReview(w http.ResponseWriter, r *http.Request) {

}