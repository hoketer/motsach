package controllers

import (
	"../models"
	"../views"
	"fmt"
	"html/template"
	"net/http"
	"strconv"
)

type ReviewController struct {
	models.Review
	views.ReviewView
}

func ReviewHandler(w http.ResponseWriter, r *http.Request) {
	rc := NewReviewController()
	rc.ControlSelectReview(w, r)
}

func NewReviewController() *ReviewController {
	return &ReviewController{}
}

func (rc ReviewController) ControlSelectReview(w http.ResponseWriter, r *http.Request) {
	t, _ := template.ParseFiles("test/get_review_by_id_test.html")
	t.Execute(w, "")
	//w.Write([]byte(r.URL.Path))
	r.ParseForm()

	if r.Form.Get("review_id") != "" {
		review_id, _ := strconv.Atoi(r.Form[`review_id`][0])
		rc.GetReviewbyID(review_id)
		//bookB, _ := json.Marshal(book)
		fmt.Println("id | name | des | created ")
		fmt.Fprintf(w, "id | name | des | created <br>")
		fmt.Printf("%3v | %8v | %6v | %6v\n", rc.Review_id, rc.Title, rc.Content, rc.Created_date)
		fmt.Fprintf(w, "%3v | %8v | %6v | %6v\n", rc.Review_id, rc.Title, rc.Content, rc.Created_date)
	}
}

