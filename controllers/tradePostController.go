package controllers

import (
	"../models"
	"../views"
	"net/http"
	"strconv"
	"fmt"
	"html/template"
)

type TradePostController struct {
	models.TradePost
	views.TradePostView
}

func TradePostHandler(w http.ResponseWriter, r *http.Request) {
	rc := NewReviewController()
	rc.ControlSelectReview(w, r)
}

func NewTradePostHandler() *TradePostController {
	return &TradePostController{}
}

func (tpc TradePostController) ControlSelectTradePost(w http.ResponseWriter, r *http.Request) {
	t, _ := template.ParseFiles("test/get_trade_post_id_test.html")
	t.Execute(w, "")
	//w.Write([]byte(r.URL.Path))
	r.ParseForm()

	if r.Form.Get("trade_post_id") != "" {
		tradePostID, _ := strconv.Atoi(r.Form[`trade_post_id`][0])
		tpc.GetTradePostByID(tradePostID)
		//bookB, _ := json.Marshal(book)
		fmt.Println("id | name | des | created ")
		fmt.Fprintf(w, "id | name | des | created <br>")
		fmt.Printf("%3v | %8v | %6v | %6v\n", tpc.TradePost_ID, tpc.Format, tpc.Description, tpc.Created_date)
		fmt.Fprintf(w, "%3v | %8v | %6v | %6v\n", tpc.TradePost_ID, tpc.Format, tpc.Description, tpc.Created_date)
	}
}
