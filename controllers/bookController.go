package controllers

import (
	"../models"
	"../views"
	"fmt"
	"html/template"
	"net/http"
	"strconv"
)

type BookController struct {
	models.Book
	views.BookView
}

func BookHandler(w http.ResponseWriter, r *http.Request) {
	bc := NewBookController()
	bc.ControlSelectBook(w, r)
}

func NewBookController() *BookController {
	return &BookController{}
}

func (bc BookController) ControlSelectBook(w http.ResponseWriter, r *http.Request) {
	t, _ := template.ParseFiles("test/get_book_by_id_test.html")
	t.Execute(w, "")
	//w.Write([]byte(r.URL.Path))
	r.ParseForm()

	if r.Form.Get("book_id") != "" {
		book_id, _ := strconv.Atoi(r.Form[`book_id`][0])
		bc.GetBookbyID(book_id)
		//bookB, _ := json.Marshal(book)
		bc.PrintBookInfo(bc.Book, w, r)
	}
}

func (bc BookController) ControlCreateBook(w http.ResponseWriter, r *http.Request) {
	t, _ := template.ParseFiles("test/create_book_test.html")
	t.Execute(w, "")
	r.ParseForm()

	if r.Form.Get("book_name") != "" {
		fmt.Print(`get`)
		book_id, _ := strconv.Atoi(r.Form[`book_id`][0])
		bc.Book_ID = book_id
		bc.Book_name = r.Form.Get(`book_name`)
		bc.Author_name = r.Form.Get(`author_name`)
		bc.Description = r.Form.Get(`description`)
		pages, _ := strconv.Atoi(r.Form.Get(`pages`))
		bc.Pages = pages
		err := bc.SaveBook()
		fmt.Print(err)
		if err != nil {
			panic(err)
		}
	}
}

func (bc BookController) ControlUpdateBook(w http.ResponseWriter, r *http.Request) {
	t, _ := template.ParseFiles("test/update_book_test.html")
	t.Execute(w, "")
	r.ParseForm()

	if r.Form.Get("book_name") != "" {
		fmt.Print(`get`)
		book_id, _ := strconv.Atoi(r.Form[`book_id`][0])
		bc.Book_ID = book_id
		bc.Book_name = r.Form.Get(`book_name`)
		bc.Author_name = r.Form.Get(`author_name`)
		bc.Description = r.Form.Get(`description`)
		pages, _ := strconv.Atoi(r.Form.Get(`pages`))
		bc.Pages = pages
		err := bc.UpdateBook()
		fmt.Print(err)
		if err != nil {
			panic(err)
		}
	}
}
