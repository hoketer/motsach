package views

import (
	"../models"
	"fmt"
	"net/http"
)

type BookView struct{}

func (_ BookView) PrintBookInfo(book models.Book, w http.ResponseWriter, r *http.Request) {
	fmt.Println("id | name | des | created ")
	fmt.Fprintf(w, "id | name | des | created <br>")
	fmt.Printf("%3v | %8v | %6v | %6v\n", book.Book_ID, book.Book_name, book.Description, book.Published_date)
	fmt.Fprintf(w, "%3v | %8v | %6v | %6v\n", book.Book_ID, book.Book_name, book.Description, book.Published_date)
}
