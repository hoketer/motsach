package views

import (
	"net/http"
	"fmt"
	"../models"
)

type ReviewView struct{}

func (_ ReviewView) PrintReviewInfo(review models.Review, w http.ResponseWriter, r *http.Request) {
	fmt.Println("Review View")
}