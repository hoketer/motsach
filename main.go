package main

import (
	"log"
	"net/http"
	//"./models"
	"./controllers"
)

func ShowCompleteTasksFunc(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte(r.URL.Path))
}

func GetTaskFunc(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		id := r.URL.Path[len("/tasks/"):]
		w.Write([]byte("Get the task " + id))
	}
}

func main() {
	PORT := "127.0.0.1:8080"
	log.Print("Running server on " + PORT)

	http.HandleFunc("/book/", controllers.BookHandler)
	//http.HandleFunc("/create_book/", controllers.Add_book_test)
	//http.HandleFunc("/update_book/", controllers.Update_book_test)

	http.HandleFunc("/review/", controllers.ReviewHandler)

	http.HandleFunc("/complete/", ShowCompleteTasksFunc)
	http.HandleFunc("/tasks/", GetTaskFunc)
	http.Handle("/static/", http.FileServer(http.Dir("/public/transistor_wallpaper")))
	log.Fatal(http.ListenAndServe(PORT, nil))
}
